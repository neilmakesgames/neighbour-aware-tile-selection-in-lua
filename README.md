# Neighbour Aware Tile Selection in Lua

A simple demo showing auto-tiling of an environment's spritesheet.

## Usage

Click to add and remove tiles.

Press `R` to generate a new random layout.

## Credits

Based on the [Neighbour Aware Tile Selection](http://www.saltgames.com/article/awareTiles/) tutorial by Sam Driver, Creative Commons licensed CC0. ([website](http://www.saltgames.com/), [twitter](https://twitter.com/SamDriver_))

Tiles by [Agnes Heyer](http://www.retinaleclipse.com/), Creative Commons licensed BY-NC-SA. [Source](https://forums.tigsource.com/index.php?topic=14166.0)

## License

This code, like the Sam Driver tutorial it is based on, is licensed [Creative Commons CC0](https://creativecommons.org/publicdomain/zero/1.0/).
