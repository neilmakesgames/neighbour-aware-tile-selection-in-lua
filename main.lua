-- main.lua
-----------------------------------------------------------
-- SETUP
-----------------------------------------------------------
function init()
  lg.setBackgroundColor(155,173,183)
  tilesize = 16
  image = lg.newImage('numberedStone.png')
  quads = setUpQuads()
  reset()
end

function reset()
  map = createMap(10, 40, 25, 25)
end

function createMap(x, y, w, h)
  local map = {}
  local probability = .3
  map.x = x
  map.y = y
  map.width = w
  map.height = h
  map.tiles = {}
  for ty = 1, h do
    map.tiles[ty] = {}
    for tx = 1, w do
      local tile = math.random() < probability and 1 or 0
      map.tiles[ty][tx] = tile
    end
  end
  return map
end

function setUpQuads()
  local quads = {}
  local x, y = 0, 0
  local w, h = tilesize, tilesize
  local sw, sh = image:getDimensions()

  for i=0,15 do
    quads[i] = lg.newQuad(x, y, w, h, sw, sh)
    x = x + tilesize
  end
  return quads
end

-----------------------------------------------------------
-- DRAWING
-----------------------------------------------------------
function drawMap()
  local w, h = map.width, map.height
  local mapX, mapY = map.x, map.y
  for y = 1, h do
    for x = 1, w do
      local drawX = mapX + (x-1) * tilesize
      local drawY = mapY + (y-1) * tilesize

      local index = getTileNeighbors(x, y)
      local val = map.tiles[y][x]

      if val ~= 0 then
        drawTile(index, drawX, drawY)
      end
    end
  end
  lg.setColor(0,255,0)
  lg.rectangle('line', mapX, mapY, w * tilesize, h * tilesize)
end

function drawTile(index, x, y)
  lg.draw(image, quads[index], x, y)
  -- uncomment this line for some debugging number printing:
  --lg.print(index, x, y)
end
-----------------------------------------------------------
-- TILE TOGGLING
-----------------------------------------------------------
function toggleMapTile(mouseX, mouseY)
  local mx, my, mw, mh = map.x, map.y, map.width * tilesize, map.height * tilesize
  if pointInRect(mouseX, mouseY, mx, my, mw, mh) then
    local x,y = mouseCoordsToMapXY(mouseX, mouseY)
    if map.tiles[y][x] == 0 then
      map.tiles[y][x] = 1
    else
      map.tiles[y][x] = 0
    end
  end
end

function pointInRect(x1, y1, x2, y2, w, h)
  return  x1 >= x2 and
          x1 <= x2 + w and
          y1 >= y2 and
          y1 <= y2 + h
end

function mouseCoordsToMapXY(mouseX, mouseY)
  -- returns 1-indexed x, y (just like the map or a lua table)
  local x, y
  x = math.floor((mouseX - map.x) / tilesize) + 1
  y = math.floor((mouseY - map.y) / tilesize) + 1
  return x, y
end

function getTileNeighbors(tileX, tileY)
  local sum = 0
  local tiles = map.tiles
  if getTile(tileY - 1, tileX    ) == 1 then sum = sum + 1 end
  if getTile(tileY    , tileX - 1) == 1 then sum = sum + 2 end
  if getTile(tileY + 1, tileX    ) == 1 then sum = sum + 4 end
  if getTile(tileY    , tileX + 1) == 1 then sum = sum + 8 end
  return sum
end

function getTile(y, x)
  local tiles = map.tiles
  if x >= 1 and x <= #tiles[1] and
     y >= 1 and y <= #tiles then
    return tiles[y][x]
  else
    return 0
  end
end
-----------------------------------------------------------
-- LOVE CORE FUNCTIONS
-----------------------------------------------------------
function love.load(arg)
  -- declare shorthand framework names
  lg = love.graphics
  li = love.image
  la = love.audio
  lm = love.mouse
  lk = love.keyboard
  lt = love.timer
  le = love.event
  ls = love.system
  lw = love.window
  lf = love.filesystem

  init()

end

function love.update(dt)

end

function love.draw()
  lg.setColor(255,255,255)
  lg.print('Click to add and remove tiles. Press "R" to randomize the layout.', 10, 10)
  drawMap()
end

function love.keypressed(key)
  if key == 'escape' then
    le.quit()
  elseif key == 'r' then
    reset()
  end
end

function love.mousepressed(x, y, button, istouch)
  toggleMapTile(x, y)
end
