-- Configuration
function love.conf(t)
	t.title = 'Neighbour Aware Tile Selection in Lua'
	t.version = '0.10.1'
	t.window.width = 420
	t.window.height = 460
end
